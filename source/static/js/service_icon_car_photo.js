$(document).ready(function() {
    var serviceTypes = JSON.parse('{{ service_types|escapejs|safe }}');
    function updateIcon() {
        var selectedServiceType = $("#id_service_type").val();
        var serviceType = serviceTypes.find(function(serviceType) {
            return serviceType.id == selectedServiceType;
        });
        if (serviceType) {
            var iconUrl = '/uploads/' + serviceType.icon;
            $('#service-icon').attr('src', iconUrl);
        }
    }

    $('#id_service_type').change(updateIcon);

    updateIcon();
});
$(document).ready(function() {
    var cars = JSON.parse('{{ cars|escapejs|safe }}');
    function updateCarCard() {
        var selectedCar = $("#id_car").val();
        var car = cars.find(function(car) {
            return car.id == selectedCar;
        });
        if (car) {
            $('#car_card').show();
            $('#car_photo').attr('src', car.car_photo);
            $('#car_title').text(car.title);
            $('#car_state_number').text(car.state_number);
            $('#car_car_mileage').text(car.car_mileage);
        } else {
            $('#car_card').hide();
        }
    }

    $('#id_car').change(updateCarCard);

    updateCarCard();
});