from django.db import models
from garage.models import Car


class ServiceType(models.Model):
    title = models.CharField(max_length=100, unique=True)
    icon = models.ImageField(upload_to='services/icon/', verbose_name='services_icon', null=True, blank=True)

    def __str__(self):
        return self.title


class Service(models.Model):
    service_type = models.ForeignKey(ServiceType, on_delete=models.PROTECT, null=False, blank=False)
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    car_mileage = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    edited_at = models.DateTimeField(auto_now=True)
    part_cost = models.DecimalField(max_digits=10, decimal_places=2)
    cost_of_work = models.DecimalField(max_digits=10, decimal_places=2)
    total_price = models.DecimalField(max_digits=10, decimal_places=2)
    part_title = models.CharField(max_length=200)
    comment = models.TextField()
    photo = models.ImageField(upload_to='services/', verbose_name='services_photo', null=True, blank=True)
    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.title
# Create your models here.
