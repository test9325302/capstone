from django.urls import path
from .views import ServicesListView, ServiceDetailView, ServiceUpdateView, ServiceCreateView, ServiceDeleteView
urlpatterns = [
    path('list/', ServicesListView.as_view(), name='services_list'),
    path('detail/<int:pk>', ServiceDetailView.as_view(), name='service_detail'),
    path('update/<int:pk>', ServiceUpdateView.as_view(), name='service_update'),
    path('create/', ServiceCreateView.as_view(), name='service_create'),
    path('delete/<int:pk>', ServiceDeleteView.as_view(), name='service_delete'),

]