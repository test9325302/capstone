from django import forms

from garage.models import Car
from .models import Service


class ServiceForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(ServiceForm, self).__init__(*args, **kwargs)
        self.fields['car'] = forms.ModelChoiceField(
            queryset=Car.objects.filter(user=user, is_deleted=False),
            widget=forms.Select(attrs={'class': 'form-control'})
        )

    class Meta:
        model = Service
        fields = ['service_type', 'car', 'title', 'car_mileage', 'part_cost', 'cost_of_work',
                  'total_price', 'part_title', 'comment', 'photo']
        widgets = {
            'service_type': forms.Select(attrs={'class': 'form-control'}),
            'title': forms.TextInput(attrs={'class': 'form-control', 'maxlength': '100'}),
            'car_mileage': forms.NumberInput(attrs={'class': 'form-control', 'min': '1', 'max': '1000000'}),
            'cost_of_work': forms.NumberInput(attrs={'class': 'form-control', 'min': '1', 'max': '1000000'}),
            'part_cost': forms.NumberInput(attrs={'class': 'form-control', 'min': '1', 'max': '1000000'}),
            'total_price': forms.NumberInput(attrs={'class': 'form-control', 'min': '1', 'max': '1000000'}),
            'part_title': forms.TextInput(attrs={'class': 'form-control', 'maxlength': '100'}),
            'comment': forms.Textarea(attrs={'class': 'form-control', 'maxlength': '300'}),
            'photo': forms.ClearableFileInput(attrs={'class': 'form-control'}),
        }
