# Generated by Django 5.0.3 on 2024-04-24 15:28

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('garage', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('car_mileage', models.IntegerField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('edited_at', models.DateTimeField(auto_now=True)),
                ('part_cost', models.DecimalField(decimal_places=2, max_digits=10)),
                ('cost_of_work', models.DecimalField(decimal_places=2, max_digits=10)),
                ('total_price', models.DecimalField(decimal_places=2, max_digits=10)),
                ('part_title', models.CharField(max_length=200)),
                ('comment', models.TextField()),
                ('photo', models.ImageField(upload_to='services/', verbose_name='services_photo')),
                ('is_deleted', models.BooleanField(default=False)),
                ('car', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='garage.car')),
            ],
        ),
    ]
