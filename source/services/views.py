import json

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import ListView, DetailView, UpdateView, CreateView
from garage.models import Car, CarMileageHistory
from .forms import ServiceForm
from .models import Service, ServiceType


class BaseView:
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        service_types = ServiceType.objects.all()
        service_types_list = list(service_types.values('id', 'icon'))
        context['service_types'] = json.dumps(service_types_list)
        cars = Car.objects.filter(user=self.request.user)
        cars_list = []
        for car in cars:
            car_dict = {
                'id': car.id,
                'title': car.title,
                'state_number': car.state_number,
                'car_mileage': car.car_mileage,
            }
            if car.car_photo:
                car_dict['car_photo'] = self.request.build_absolute_uri(car.car_photo.url)
            cars_list.append(car_dict)
        context['cars'] = json.dumps(cars_list)
        return context


class ServiceBaseView(BaseView):
    pass


class ServicesListView(LoginRequiredMixin, ListView):
    model = Service
    template_name = 'services/service_list.html'
    context_object_name = 'services'
    paginate_by = 6
    paginate_orphans = 0

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cars'] = Car.objects.filter(user=self.request.user, is_deleted=False)
        return context

    def get_queryset(self):
        #  что пользователь видит только свои услуги
        car_id = self.request.GET.get('car_id')
        if car_id:
            return Service.objects.filter(car__user=self.request.user, is_deleted=False, car_id=car_id)
        else:
            return Service.objects.filter(car__user=self.request.user, is_deleted=False)


class ServiceDetailView(LoginRequiredMixin, DetailView):
    model = Service
    template_name = 'services/service_detail.html'
    context_object_name = 'service'

    def get_queryset(self):
        #  что пользователь видит только свои услуги
        return Service.objects.filter(car__user=self.request.user)


class ServiceUpdateView(LoginRequiredMixin, PermissionRequiredMixin, ServiceBaseView, UpdateView):
    model = Service
    form_class = ServiceForm
    template_name = 'services/service_update_form.html'
    permission_required = 'services.change_service'
    success_url = reverse_lazy('services_list')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user  # Передаем текущего пользователя в kwargs
        return kwargs

    def get_queryset(self):
        #  что пользователь редактирует только свои услуги
        return Service.objects.filter(car__user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['is_editing'] = True
        return context

    def form_valid(self, form):
        # Получаем экземпляр текущей услуги
        instance = form.save(commit=False)
        # Получаем экземпляр автомобиля, связанного с текущей услугой
        car = instance.car
        # Получаем все записи в истории пробега для данного автомобиля
        all_mileage_history = CarMileageHistory.objects.filter(car=car, is_deleted=False)
        # Получаем последнюю запись в истории пробега для данного автомобиля
        last_mileage_history = all_mileage_history.last()
        # Проверяем, является ли текущая услуга последней в истории пробега
        is_last_service = last_mileage_history and last_mileage_history.parent_object == instance
        # Если пробег был изменен и текущая услуга является последней в истории пробега
        if int(form.cleaned_data['car_mileage']) != car.car_mileage and is_last_service:
            # Обновляем пробег автомобиля на значение из формы
            car.car_mileage = form.cleaned_data['car_mileage']
            car.save()
        # Сохраняем изменения в текущей услуге
        instance.save()
        # Создаем новую запись в истории пробега после редактирования услуги
        CarMileageHistory.objects.create(
            car=car,
            car_mileage=form.cleaned_data['car_mileage'],
            content_type=ContentType.objects.get_for_model(Service),
            object_id=instance.id
        )

        return HttpResponseRedirect(self.get_success_url())


class ServiceCreateView(LoginRequiredMixin, PermissionRequiredMixin, ServiceBaseView, CreateView):
    model = Service
    form_class = ServiceForm
    template_name = 'services/service_update_form.html'
    permission_required = 'services.add_service'
    success_url = reverse_lazy('services_list')

    def get_form_kwargs(self):
        kwargs = super(ServiceCreateView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['is_editing'] = False
        return context

    def form_valid(self, form):
        response = super().form_valid(form)

        # Получаем экземпляр автомобиля, связанного с текущей услугой
        car = self.object.car

        # Обновляем пробег автомобиля на указанное значение в услуге
        car.car_mileage = form.cleaned_data['car_mileage']
        car.save()

        # Создаем запись в CarMileageHistory после создания услуги
        CarMileageHistory.objects.create(
            car=car,
            car_mileage=car.car_mileage,
            content_type=ContentType.objects.get_for_model(Service),
            object_id=self.object.id
        )

        return response


class ServiceDeleteView(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'services.delete_service'

    def get(self, request, *args, **kwargs):
        # Получаем объект удаляемой услуги
        service = get_object_or_404(Service, pk=kwargs['pk'], car__user=request.user)

        # Обновляем флаг is_deleted для услуги
        service.is_deleted = True
        service.save()

        # Находим связанные записи в CarMileageHistory и устанавливаем is_deleted = True
        CarMileageHistory.objects.filter(
            content_type=ContentType.objects.get_for_model(Service),
            object_id=service.id
        ).update(is_deleted=True)

        # Обновляем пробег у автомобиля
        car = service.car
        last_mileage_history = CarMileageHistory.objects.filter(
            car=car,
            is_deleted=False
        ).last()

        if last_mileage_history:
            car.car_mileage = last_mileage_history.car_mileage
            car.save()

        return redirect('services_list')
