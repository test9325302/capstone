from django.contrib.auth import get_user_model
from django.db import models
from garage.models import Car
from services.models import ServiceType


class Notification(models.Model):
    service_type = models.ForeignKey(ServiceType, on_delete=models.SET_NULL, null=True, blank=True)
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    edited_at = models.DateTimeField(auto_now=True)
    reminder_date = models.DateTimeField(null=True, blank=True)
    comment = models.TextField(null=True, blank=True)
    car_mileage_notifications = models.IntegerField(null=False, blank=False)
    is_active = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False)
    turn_on = models.BooleanField(default=False)
    user = models.ForeignKey(get_user_model(), on_delete=models.PROTECT)

    def __str__(self):
        return self.title
