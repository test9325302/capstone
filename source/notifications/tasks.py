import logging
from celery import shared_task
from django.utils import timezone
from .models import Notification, Car

logger = logging.getLogger(__name__)


@shared_task
def check_notifications():
    now = timezone.now()
    logger.info('Start check notification')
    for notification in Notification.objects.filter(is_deleted=False, is_active=True):
        if notification.reminder_date and notification.reminder_date <= now:
            notification.turn_on = True
            notification.save()
        if notification.car.car_mileage > notification.car_mileage_notifications:
            notification.turn_on = True
            notification.save()
    logger.info('END check notification')
