from django import forms
from garage.models import Car
from services.models import ServiceType
from .models import Notification


class NotificationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(NotificationForm, self).__init__(*args, **kwargs)
        self.fields['car'] = forms.ModelChoiceField(
            queryset=Car.objects.filter(user=user, is_deleted=False),
            widget=forms.Select(attrs={'class': 'form-control'})
        )
        self.fields['service_type'] = forms.ModelChoiceField(
            queryset=ServiceType.objects.all(),  # Фильтровать доступные типы услуг можно здесь
            widget=forms.Select(attrs={'class': 'form-control'})
        )

    class Meta:
        model = Notification
        fields = ['service_type', 'car', 'title', 'reminder_date', 'comment', 'car_mileage_notifications', 'is_active']
        labels = {
            'is_active': 'Reminder enabled',
        }
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control', 'maxlength': '100'}),
            'reminder_date': forms.DateTimeInput(attrs={'class': 'form-control', 'type': 'datetime-local'}),
            'comment': forms.Textarea(attrs={'class': 'form-control', 'maxlength': '300'}),
            'car_mileage_notifications': forms.NumberInput(
                attrs={'class': 'form-control', 'min': '1', 'max': '1000000'}),
            'is_active': forms.CheckboxInput(
                attrs={'class': 'form-check-input', 'type': 'checkbox', 'data-bs-toggle': 'toggle'}),
        }
