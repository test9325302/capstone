from django.urls import path
from .views import NotificationsListView, NotificationsDetailView, NotificationCreateView, NotificationUpdateView,\
    NotificationsDeleteView, NotificationTurnoffView
urlpatterns = [
    path('list/', NotificationsListView.as_view(), name='notifications_list'),
    path('detail/<int:pk>/', NotificationsDetailView.as_view(), name='notifications_detail'),
    path('create/', NotificationCreateView.as_view(), name='notifications_create'),
    path('update/<int:pk>/', NotificationUpdateView.as_view(), name='notifications_update'),
    path('delete/<int:pk>/', NotificationsDeleteView.as_view(), name='notifications_delete'),
    path('turnoff/<int:pk>/', NotificationTurnoffView.as_view(), name='notification_turnoff'),
]