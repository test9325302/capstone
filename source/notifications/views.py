from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import get_object_or_404, redirect
from django.views.generic import ListView, DetailView, UpdateView, CreateView
from django.views import View
from django.urls import reverse_lazy

from garage.models import Car
from notifications.forms import NotificationForm
from notifications.models import Notification
from services.views import BaseView
from .tasks import check_notifications


class NotificationBaseView(BaseView):
    pass


# Create your views here.
class NotificationsDeleteView(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'notifications.delete_notification'

    def get(self, request, *args, **kwargs):
        notification = get_object_or_404(Notification, pk=kwargs['pk'], user=request.user)
        notification.is_deleted = True
        notification.is_active = False
        notification.turn_on = False
        notification.save()
        return redirect('notifications_list')


class NotificationCreateView(LoginRequiredMixin, PermissionRequiredMixin, NotificationBaseView, CreateView):
    model = Notification
    form_class = NotificationForm
    permission_required = 'notifications.add_notification'
    template_name = 'notifications/notifications_create_update.html'
    success_url = reverse_lazy('notifications_list')

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['is_editing'] = False
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user  # Передаем текущего пользователя в форму
        return kwargs


class NotificationUpdateView(LoginRequiredMixin, PermissionRequiredMixin, NotificationBaseView, UpdateView):
    model = Notification
    form_class = NotificationForm
    permission_required = 'notifications.change_notification'
    template_name = 'notifications/notifications_create_update.html'
    success_url = reverse_lazy('notifications_list')

    def form_valid(self, form):
        notification = form.save(commit=False)
        if not notification.is_active:
            notification.turn_on = False
        notification.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['is_editing'] = True
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user  # Передаем текущего пользователя в форму
        return kwargs


class NotificationsDetailView(LoginRequiredMixin, DetailView):
    model = Notification
    template_name = 'notifications/notification_detail.html'
    context_object_name = 'notifications'

    def get_queryset(self):
        # Получаем все уведомления, связанные с текущим пользователем
        return Notification.objects.filter(user=self.request.user)


class NotificationsListView(LoginRequiredMixin, ListView):
    model = Notification
    template_name = 'notifications/notification_list.html'
    context_object_name = 'notifications'  # Имя контекстного объекта, который будет передан в шаблон
    paginate_by = 6
    paginate_orphans = 0

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cars'] = Car.objects.filter(user=self.request.user, is_deleted=False)
        return context

    def get_queryset(self):
        # Получаем все уведомления, связанные с текущим пользователем и которые не были удалены
        check_notifications()
        car_id = self.request.GET.get('car_id')
        if car_id:
            return Notification.objects.filter(user=self.request.user, is_deleted=False, car_id=car_id)
        else:
            return Notification.objects.filter(user=self.request.user, is_deleted=False)


class NotificationTurnoffView(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'notifications.change_notification'

    def get(self, request, *args, **kwargs):
        notification = get_object_or_404(Notification, pk=kwargs['pk'], user=request.user)
        notification.turn_on = False
        notification.is_active = False
        notification.save()
        check_notifications()
        return redirect('notifications_list')
