from .models import Notification


def notifications(request):
    if request.user.is_authenticated:
        reminders = Notification.objects.filter(user=request.user, turn_on=True)
        return {'reminders': reminders}
    return {}
