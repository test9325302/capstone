from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from celery.schedules import crontab

#   окружения DJANGO_SETTINGS_MODULE по умолчанию.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'source.settings')

app = Celery('source')

app.conf.broker_connection_retry_on_startup = True

# автоматически настроен для использования Django.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Загрузить модули задач из всех зарегистрированных приложений Django.
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'check-notifications-every-hour': {
        'task': 'notifications.tasks.check_notifications',
        'schedule': crontab(),  # Запуск каждый час
    },
}
