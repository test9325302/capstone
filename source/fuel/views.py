from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import ListView, CreateView, UpdateView
from django.contrib.contenttypes.models import ContentType
from garage.models import CarMileageHistory
from .models import Fuel, Car
from .forms import FuelForm
import json


class BaseView:
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        cars = Car.objects.filter(user=self.request.user)
        cars_list = []
        for car in cars:
            car_dict = {
                'id': car.id,
                'title': car.title,
                'state_number': car.state_number,
                'car_mileage': car.car_mileage,
            }
            if car.car_photo:
                car_dict['car_photo'] = self.request.build_absolute_uri(car.car_photo.url)
            cars_list.append(car_dict)
        context['cars'] = json.dumps(cars_list)
        return context


class FuelBaseView(BaseView):
    pass


class FuelListView(LoginRequiredMixin, ListView):
    model = Fuel
    template_name = 'fuel/fuel_list.html'
    context_object_name = 'fuels'
    paginate_by = 6
    paginate_orphans = 0

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cars'] = Car.objects.filter(user=self.request.user, is_deleted=False)
        return context

    def get_queryset(self):
        car_id = self.request.GET.get('car_id')
        if car_id:
            fuels = Fuel.objects.filter(car__user=self.request.user, car_id=car_id)
        else:
            fuels = Fuel.objects.filter(car__user=self.request.user)

        for fuel in fuels:
            # Получаем предыдущую запись Fuel для данного автомобиля, которая не была удалена
            previous_fuel = Fuel.objects.filter(car=fuel.car, created_at__lt=fuel.created_at,
                                                is_deleted=False).order_by('-created_at').first()

            # Если есть предыдущая запись Fuel
            if previous_fuel:
                # Вычисляем разницу в пробеге
                mileage_difference = fuel.car_mileage - previous_fuel.car_mileage

                # Вычисляем средний расход топлива на 100 км и округляем до двух десятичных знаков
                fuel.average_fuel_consumption = round((fuel.fuel_volume / mileage_difference * 100),
                                                      2) if mileage_difference else 0
            else:
                fuel.average_fuel_consumption = 0

        return fuels


class FuelCreateView(LoginRequiredMixin, PermissionRequiredMixin, FuelBaseView, CreateView):
    model = Fuel
    form_class = FuelForm
    template_name = 'fuel/fuel_update_form.html'
    permission_required = 'fuel.add_fuel'
    success_url = reverse_lazy('fuel_list')

    def get_form_kwargs(self):
        kwargs = super(FuelCreateView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['is_editing'] = False
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()

        # Получаем экземпляр автомобиля, связанного с текущим заправкой
        car = self.object.car

        # Обновляем пробег автомобиля на указанное значение в заправке
        car.car_mileage = form.cleaned_data['car_mileage']
        car.save()

        # Создаем запись в CarMileageHistory после создания заправки
        CarMileageHistory.objects.create(
            car=car,
            car_mileage=car.car_mileage,
            content_type=ContentType.objects.get_for_model(Fuel),
            object_id=self.object.id
        )

        return super().form_valid(form)


class FuelUpdateView(LoginRequiredMixin, PermissionRequiredMixin, FuelBaseView, UpdateView):
    model = Fuel
    form_class = FuelForm
    template_name = 'fuel/fuel_update_form.html'
    permission_required = 'fuel.change_fuel'
    success_url = reverse_lazy('fuel_list')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user  # Передаем текущего пользователя в kwargs
        return kwargs

    def get_queryset(self):
        #  что пользователь редактирует только свои заправки
        return Fuel.objects.filter(car__user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['is_editing'] = True
        return context

    def form_valid(self, form):
        # Получаем экземпляр текущей заправки
        instance = form.save(commit=False)
        # Получаем экземпляр автомобиля, связанного с текущей заправкой
        car = instance.car
        # Получаем все записи в истории пробега для данного автомобиля
        all_mileage_history = CarMileageHistory.objects.filter(car=car, is_deleted=False)
        # Получаем последнюю запись в истории пробега для данного автомобиля
        last_mileage_history = all_mileage_history.last()
        # Проверяем, является ли текущая заправка последней в истории пробега
        is_last_fuel = last_mileage_history and last_mileage_history.parent_object == instance
        # Если пробег был изменен и текущая заправка является последней в истории пробега
        if int(form.cleaned_data['car_mileage']) != car.car_mileage and is_last_fuel:
            # Обновляем пробег автомобиля на значение из формы
            car.car_mileage = form.cleaned_data['car_mileage']
            car.save()
        # Сохраняем изменения в текущей заправке
        instance.save()
        # Создаем новую запись в истории пробега после редактирования заправки
        CarMileageHistory.objects.create(
            car=car,
            car_mileage=form.cleaned_data['car_mileage'],
            content_type=ContentType.objects.get_for_model(Fuel),
            object_id=instance.id
        )

        return HttpResponseRedirect(self.get_success_url())


class FuelDeleteView(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'fuel.delete_fuel'

    def get(self, request, *args, **kwargs):
        # Получаем объект удаляемой заправки
        fuel = get_object_or_404(Fuel, pk=kwargs['pk'], car__user=request.user)

        # Обновляем флаг is_deleted для заправки
        fuel.is_deleted = True
        fuel.save()

        # Находим связанные записи в CarMileageHistory и устанавливаем is_deleted = True
        CarMileageHistory.objects.filter(
            content_type=ContentType.objects.get_for_model(Fuel),
            object_id=fuel.id
        ).update(is_deleted=True)

        # Обновляем пробег у автомобиля
        car = fuel.car
        last_mileage_history = CarMileageHistory.objects.filter(
            car=car,
            is_deleted=False
        ).last()

        if last_mileage_history:
            car.car_mileage = last_mileage_history.car_mileage
            car.save()

        return redirect('fuel_list')
