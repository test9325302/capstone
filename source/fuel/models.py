from django.db import models
from django.contrib.auth import get_user_model

from garage.models import Car


class FuelType(models.Model):
    FUEL_TYPES = (
        ('92', '92'),
        ('95', '95'),
        ('98', '98'),
        ('Diesel', 'Diesel'),
    )
    fuel_type = models.CharField(max_length=50, choices=FUEL_TYPES)
    fuel_photo = models.ImageField(upload_to='fuels/', verbose_name='fuel_photo', null=True, blank=True)

    def __str__(self):
        return self.fuel_type


class Fuel(models.Model):
    fuel_type = models.ForeignKey(FuelType, on_delete=models.SET_NULL, null=True, blank=True)
    price_per_liter = models.DecimalField(max_digits=5, decimal_places=2)
    fuel_volume = models.DecimalField(max_digits=5, decimal_places=2)
    total_amount = models.DecimalField(max_digits=10, decimal_places=2)
    car_mileage = models.IntegerField(null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    edited_at = models.DateTimeField(auto_now=True)

    is_deleted = models.BooleanField(default=False)
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    user = models.ForeignKey(get_user_model(), on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.fuel_type} - {self.car}'
