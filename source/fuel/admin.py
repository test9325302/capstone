from django.contrib import admin
from .models import Fuel , FuelType

admin.site.register(Fuel)
admin.site.register(FuelType)
# Register your models here.
