from django.urls import path
from .views import FuelListView, FuelCreateView, FuelUpdateView, FuelDeleteView
urlpatterns = [
    path('list/', FuelListView.as_view(), name='fuel_list'),
    # path('detail/<int:pk>', ServiceDetailView.as_view(), name='service_detail'),
    path('update/<int:pk>', FuelUpdateView.as_view(), name='fuel_update'),
    path('create/', FuelCreateView.as_view(), name='fuel_create'),
    path('delete/<int:pk>', FuelDeleteView.as_view(), name='fuel_delete'),

]