from django import forms
from garage.models import Car
from .models import Fuel


class FuelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(FuelForm, self).__init__(*args, **kwargs)
        self.fields['car'] = forms.ModelChoiceField(
            queryset=Car.objects.filter(user=user, is_deleted=False),
            widget=forms.Select(attrs={'class': 'form-control'})
        )

    class Meta:
        model = Fuel
        fields = ['fuel_type', 'car', 'price_per_liter', 'fuel_volume', 'total_amount', 'car_mileage']
        widgets = {
            'fuel_type': forms.Select(attrs={'class': 'form-control'}),
            'price_per_liter': forms.NumberInput(attrs={'class': 'form-control', 'min': '1', 'max': '1000000'}),
            'fuel_volume': forms.NumberInput(attrs={'class': 'form-control', 'min': '1', 'max': '1000000'}),
            'total_amount': forms.NumberInput(attrs={'class': 'form-control', 'min': '1', 'max': '1000000'}),
            'car_mileage': forms.NumberInput(attrs={'class': 'form-control', 'min': '1', 'max': '1000000'}),

        }
