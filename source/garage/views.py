from django.db import transaction
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import ListView, UpdateView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from notifications.models import Notification
from notifications.tasks import check_notifications
from services.models import Service
from .models import Car, CarMileageHistory
from .forms import CarForm
from django.contrib.contenttypes.models import ContentType


class CarDeleteView(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'garage.delete_car'

    def get_object(self):
        car_id = self.kwargs.get('pk')
        return get_object_or_404(Car, id=car_id, user=self.request.user)

    def get(self, request, *args, **kwargs):
        car = self.get_object()
        self.delete_related_mileage_history(car)
        self.delete_related_services(car)
        self.delete_related_notifications(car)
        car.is_deleted = True
        car.save()
        return HttpResponseRedirect(self.get_success_url())

    # В этом обновленном коде я добавил метод delete_related_mileage_history,
    # который устанавливает is_deleted = True для всех записей CarMileageHistory,
    # связанных с удаляемым автомобилем. Я также обернул операции в методе delete_related_mileage_history в транзакцию,
    # чтобы гарантировать атомарное выполнение операций удаления.
    def delete_related_mileage_history(self, car):
        with transaction.atomic():
            mileage_history = CarMileageHistory.objects.filter(car=car)
            mileage_history.update(is_deleted=True)

    def delete_related_services(self, car):
        with transaction.atomic():
            services = Service.objects.filter(car=car, is_deleted=False)
            services.update(is_deleted=True)

    def delete_related_notifications(self, car):
        with transaction.atomic():
            notifications = Notification.objects.filter(car=car, is_deleted=False)
            notifications.update(turn_on=False)
            notifications.update(is_active=False)
            notifications.update(is_deleted=True)

    def get_success_url(self):
        return reverse_lazy('garage_list')


class CarUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = Car
    form_class = CarForm
    template_name = 'garage/car_create_form.html'
    success_url = reverse_lazy('garage_list')
    permission_required = 'garage.change_car'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['is_editing'] = True
        return context

    def form_valid(self, form):
        form.instance.user = self.request.user
        response = super().form_valid(form)

        # Создаем запись в CarMileageHistory после редактирования автомобиля
        CarMileageHistory.objects.create(
            car_mileage=form.cleaned_data['car_mileage'],
            car=form.instance,  # Указываем экземпляр автомобиля
            content_type=ContentType.objects.get_for_model(Car),
            object_id=form.instance.id
        )
        check_notifications()
        return response

    def get_queryset(self):
        #  что пользователь редактирует только свои автомобили
        qs = super().get_queryset()
        return qs.filter(user=self.request.user)


class GarageListView(LoginRequiredMixin, ListView):
    model = Car
    template_name = 'garage/car_list.html'
    context_object_name = 'cars'
    paginate_by = 6
    paginate_orphans = 0

    def get_queryset(self):
        check_notifications()
        # Получаем список автомобилей пользователя, учитывая, что они не удалены
        queryset = Car.objects.filter(user=self.request.user, is_deleted=False)

        # Для каждого автомобиля добавляем информацию о количестве непрочитанных уведомлений
        for car in queryset:
            car.unread_notifications_count = Notification.objects.filter(car=car, is_active=True).count()

        return queryset


class CarCreateView(LoginRequiredMixin, CreateView):
    model = Car
    form_class = CarForm
    template_name = 'garage/car_create_form.html'
    success_url = reverse_lazy('garage_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['is_editing'] = False
        return context

    def form_valid(self, form):
        form.instance.user = self.request.user
        response = super().form_valid(form)

        # Создаем запись в CarMileageHistory после создания автомобиля
        CarMileageHistory.objects.create(
            car_mileage=form.cleaned_data['car_mileage'],
            car=form.instance,  # Указываем экземпляр автомобиля
            content_type=ContentType.objects.get_for_model(Car),
            object_id=form.instance.id
        )

        return response


class CarMileageHistoryListView(LoginRequiredMixin, ListView):
    model = CarMileageHistory
    template_name = 'garage/car_mileage_history_list.html'  # шаблон
    context_object_name = 'car_mileage_history'
    paginate_by = 6
    paginate_orphans = 0

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['car_id'] = self.request.GET.get('car_id')
        return context

    def get_queryset(self):
        car_id = self.request.GET.get('car_id')
        if car_id:
            return CarMileageHistory.objects.filter(car__user=self.request.user, car_id=car_id)
        else:
            return CarMileageHistory.objects.filter(car__user=self.request.user)
