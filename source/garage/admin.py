from django.contrib import admin
from .models import Car, CarMileageHistory

admin.site.register(Car)
admin.site.register(CarMileageHistory)
# Register your models here.
