from django.urls import path
from .views import GarageListView, CarCreateView, CarUpdateView, CarDeleteView, CarMileageHistoryListView

urlpatterns = [
    path('car/list', GarageListView.as_view(), name='garage_list'),
    path('car/history/list', CarMileageHistoryListView.as_view(), name='car_history_list'),
    path('car/create', CarCreateView.as_view(), name='car_create'),
    path('car/<int:pk>/edit/', CarUpdateView.as_view(), name='car_edit'),
    path('car/<int:pk>/delete/', CarDeleteView.as_view(), name='car_delete'),
]
