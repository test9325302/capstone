from django import forms
from .models import Car


class CarForm(forms.ModelForm):
    class Meta:
        model = Car
        fields = ['title', 'date_of_purchase', 'state_number', 'car_mileage', 'car_photo']
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control', 'maxlength': '100'}),
            'date_of_purchase': forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'state_number': forms.TextInput(attrs={'class': 'form-control', 'maxlength': '12'}),
            'car_mileage': forms.NumberInput(attrs={'class': 'form-control', 'min': '1', 'max': '1000000'}),
            'car_photo': forms.ClearableFileInput(attrs={'class': 'form-control'}),
        }
