from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class Car(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.PROTECT)
    title = models.CharField(max_length=200)
    date_of_purchase = models.DateField()
    state_number = models.CharField(max_length=50)
    car_mileage = models.IntegerField()
    car_photo = models.ImageField(upload_to='cars/', verbose_name='car_photo', null=True, blank=True)
    is_deleted = models.BooleanField(default=False)
    edited_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


class CarMileageHistory(models.Model):
    car_mileage = models.IntegerField()
    is_deleted = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    # Эти поля вместе представляют GenericForeignKey
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    parent_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return f'Car Mileage History for {self.parent_object}'

# Create your models here.
